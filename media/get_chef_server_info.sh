#!/bin/bash
exit_usage(){
    echo "Error : $1"
    echo "Usage $0 <chef-server-url>" 
    echo "User directory must be /root"
    exit 1
}

CHEF_SERVER=$1

if [ $# -ne 1 ]; then
    exit_usage "Insufficient arguments"
fi

#echo "CHEF_SERVER : $CHEF_SERVER"
mkdir -p /home/cdms/.chef
scp root@$CHEF_SERVER:/root/.chef/knife.rb /home/cdms/.chef/knife.rb
scp root@$CHEF_SERVER:/root/.chef/root.pem /home/cdms/.chef/root.pem
sudo chmod 600 /home/cdms/.chef/root.pem

sed -i 's/\/root\//\/home\/cdms\//g' /home/cdms/.chef/knife.rb

sudo mkdir -p /etc/chef
sudo scp root@$CHEF_SERVER:/tmp/client.rb /etc/chef/
sudo scp root@$CHEF_SERVER:/tmp/validation.pem /etc/chef/client.pem
sudo chmod 600 /etc/chef/client.pem


