# coding=utf-8
# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
# Copyright 2012 Nebula, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
"""
Views for cdms project front.
"""

import json
import re

import chef
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.conf import settings
from django.views.generic import TemplateView

from horizon import messages
from horizon import exceptions
from openstack_dashboard import api
from horizon import tables
from openstack_dashboard.dashboards.project.cdms_app_create \
    import tables as project_tables
from openstack_dashboard.dashboards.project.stacks.forms import exception_to_validation_msg
from openstack_dashboard.dashboards.project.cdms.models \
    import Single


TEMPLATE_ = [  # power hard coding start.
               'CDMS-3-tier-autoscaling.template',
]  # power hard coding end.


class IndexView(tables.DataTableView):
    table_class = project_tables.ApplicationStacksTable
    template_name = 'project/cdms_app_create/index.html'

    def get_data(self):
        templates = []
        for index, file_name in enumerate(TEMPLATE_):
            template_path = settings.MEDIA_ROOT + '/heat-template/' + file_name
            templates.append(project_tables.ApplicationTemplate(index, file_name, template_path))

        return templates


class TemplateShowView(TemplateView):
    template_name = 'project/cdms_app_create/show.html'

    def get_context_data(self, **kwargs):
        data = super(TemplateShowView, self).get_context_data(**kwargs)
        index = int(kwargs['template_id'])
        template_path = settings.MEDIA_ROOT + '/heat-template/' + TEMPLATE_[index]
        with open(template_path) as p:
            data['template_text'] = p.read()
        return data


def get_server_url():
    try:
        server_url = str(Single.objects.get(key='chef_server_url').value)
    except Single.DoesNotExist:
        server_url = 'https://172.16.100.70:443'
    except Single.MultipleObjectsReturned:
        server_url = 'https://172.16.100.70:443'
    return server_url


class AppCreateView(TemplateView):
    template_name = 'project/cdms_app_create/create.html'

    def __init__(self, **kwargs):
        super(AppCreateView, self).__init__(**kwargs)
        self.param_prefix = 'CDMS'
        self.chef_role_prefix = '_c_r'

    def create_stack(self, request, data):
        prefix_length = len(self.param_prefix)
        params_list = [(k[prefix_length:], v) for (k, v) in data.iteritems()
                       if k.startswith(self.param_prefix)]

        fields = {
            'stack_name': data.get('stack_name'),
            'timeout_mins': 60,
            'disable_rollback': True,
            'parameters': dict(params_list),
            'password': data.get('password')
        }
        fields['template'] = data.get('template_data')

        try:
            stack = api.heat.stack_create(self.request, **fields)
            messages.success(request, _("Stack creation started."))
            return stack
        except Exception as e:
            msg = exception_to_validation_msg(e)
            exceptions.handle(request, msg or _('Stack creation failed.'))

    def find_leaf_and_path(self, obj, path, ret):
        if not isinstance(obj, dict):
            ret.append({'path': path, 'val': obj})
        else:
            for k, v in obj.iteritems():
                self.find_leaf_and_path(v, path + "[%s]" % k, ret)

    @staticmethod
    def set_data_with_flat_key(data, flat_key, val):
        keys = [a.strip(']') for a in flat_key.replace('[', ',').split(',')[1:]]
        key = keys.pop()
        dicPointer = data
        for k in keys:
            dicPointer = dicPointer[k]
        dicPointer[key] = val
        return dicPointer[key]

    def _get_chef_attr_json(self, request, role_name=None):
        role_name = role_name or request.GET.get('role_name', None)
        dic = {
            'role_name': role_name,
            'attr': []
        }
        if role_name:
            with chef.autoconfigure():
                dt = chef.Role(role_name).override_attributes
                self.find_leaf_and_path(dt, 'override', dic['attr'])

        return json.dumps(dic)

    def create_kwargs(self, template_data_dict):
        # Validate the template and get back the params.
        kwargs = {'template': template_data_dict}

        try:
            validated = api.heat.template_validate(self.request, **kwargs)
        except Exception as e:
            msg = exception_to_validation_msg(e)
            if not msg:  # it will be never happen!!!!!
                msg = _('An unknown problem occurred validating the template.')
            raise Exception(msg)

        kwargs = {'parameters': validated,
                  'environment_data': '',
                  'environment_url': '',
                  'template_data': json.dumps(template_data_dict),
                  'template_url': ''}
        return kwargs

    def handle_post_method(self, request):
        json_load = json.loads(request.POST['template_data'])
        if 'ChefServerAddress' in json_load['Parameters']:
            url = get_server_url()  # returns  ex) 'https://10.0.109.2:443'
            json_load['Parameters']['ChefServerAddress']['Default'] = re.findall(r'[0-9]+(?:\.[0-9]+){3}', url)[0]
        data = {
            'template_data': json_load,
            'parameters': json_load['Parameters'],
            'stack_name': request.POST['stack_name'],
            'password': request.POST['password'],
            'environment_url': u'',
            'enable_rollback': False,
            'template_url': u'',
            'environment_data': u'',
            'timeout_mins': 60,
        }
        chef_req = {}
        for k in request.POST:
            if k.startswith(self.param_prefix):
                data[k] = request.POST[k]
            if k.startswith(self.chef_role_prefix):  # chef role param extract
                kk = k[len(self.chef_role_prefix):].split('|')
                rolename = kk[0]
                chef_req.setdefault(rolename, {})[kk[1]] = request.POST[k]

        # edit chef role.
        for chef_role, val in chef_req.iteritems():
            with chef.autoconfigure():
                role = chef.Role(chef_role)
                dt = role.override_attributes
                for k, v in val.iteritems():
                    try:
                        user_input = json.loads(v)
                    except ValueError:
                        user_input = v
                    self.set_data_with_flat_key(dt, k, user_input)
                role.override_attributes = dt
                role.save()
        ret_stack = self.create_stack(request, data)
        return HttpResponseRedirect('/project/cdms/app/%s/' % ret_stack['stack']['id'])
        # return HttpResponseRedirect('/project/cdms/app/%s/?tab=Infra_details__topology' % ret_stack['stack']['id'])

    def dispatch(self, request, *args, **kwargs):
        template_name = kwargs['template_name']
        template_path = settings.MEDIA_ROOT + '/heat-template/' + template_name
        with open(template_path) as fp:
            template_json_load = json.load(fp)
        kwargs.update(self.create_kwargs(template_json_load))

        if request.is_ajax():
            return HttpResponse(self._get_chef_attr_json(request), content_type="application/json")
        if request.method.lower() == 'post':
            return self.handle_post_method(request)
        return super(AppCreateView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def _get_string_of_resource_and_param(domain_res, tartget_parms, stack_parm):
        def recursive(res, param, ret):
            exp = '{ *"Ref" *: *"%s" *}' % param
            candidate = [k for k, v in res.iteritems() if bool(re.search(exp, json.dumps(v)))]
            for c in candidate:
                if res[c]['Type'] in (
                        "CDMS::AutoScaling::AutoScalingGroup",
                        "AWS::ElasticLoadBalancing::LoadBalancer",
                        "CDMS::EC2::Instance"):
                    ret.append(c)
                else:
                    recursive(res, c, ret)

        resources = []
        res_key_param_value_dict = {}
        for t in tartget_parms:
            ret = []
            recursive(domain_res, t['label'], ret)
            for res in ret:
                res_key_param_value_dict.setdefault(res, []).append(t['label'])
                resources.append(res)

        for k in res_key_param_value_dict:
            res_key_param_value_dict[k] = set(res_key_param_value_dict[k])
            res_key_param_value_dict[k] = [{'label': x, 'val': stack_parm[x]} for x in res_key_param_value_dict[k]]

        return [(res, res_key_param_value_dict[res]) for res in set(resources)]

    @staticmethod
    def bind_policies_relative(policys, PRAM, alarms):
        def get_policy_context_for_res(res, _map):
            return _map.get(res, [])

        def find_res_of_policy(policy):
            s = json.dumps(policy["Properties"]['AutoScalingGroupName'])
            exp = '{ *"Ref" *: *"(\w+)" *}'
            return re.findall(exp, s)[0]

        def build_res_map(context):
            _map = {}
            for k in context:
                _map.setdefault(find_res_of_policy(policys[k]), []).append(context[k])
            return _map

        def find_params_of_policy(policy, params):
            s = json.dumps(policy["Properties"]['ScalingAdjustment'])
            exp = '{ *"Ref" *: *"(\w+)" *}'
            return [{'label': a, 'val': params[a]} for a in [re.findall(exp, s)[0]]]

        def find_params_of_alarm(alarm, params):
            keys = ["meter_name", "period", "threshold"]
            key_dumps = [(s, json.dumps(alarm['Properties'][s])) for s in keys]
            exp = '{ *"Ref" *: *"(\w+)" *}'
            return [{'label': a, 'val': params[a], 'type': type_key} for type_key, a in
                    [(key, re.findall(exp, s)[0]) for key, s in key_dumps]]

        def find_policy_of_alarm(alarm):
            s = json.dumps(alarm["Properties"]['alarm_actions'])
            exp = '\[ *{ *" *Fn::GetAtt *" *: *\[ *" *(\w+) *" *, *" *AlarmUrl *" *] *} *]'
            return re.findall(exp, s)[0]

        context = dict([
            (p, {
                'params': find_params_of_policy(policys[p], PRAM),
                'alarms_params': None
            }) for p in policys
        ])

        for a in alarms:
            y = find_policy_of_alarm(alarms[a])
            context[y]['alarms_params'] = find_params_of_alarm(alarms[a], PRAM)

        return get_policy_context_for_res, build_res_map(context)

    @staticmethod
    def filter_chef_role(chef_role, resource_name):
        # hardcode 201406162336 chef list
        if resource_name == 'Database':
            chef_role_regex = '(.*mysql.*)'
        else:
            chef_role_regex = '(.*jira.*)|(.*tomcat.*)'
        filtered_list = [role for role in chef_role if bool(re.search(chef_role_regex, role))]
        return filtered_list


    def get_context_data(self, **kwargs):
        context_data = super(AppCreateView, self).get_context_data(**kwargs)
        template_dict = json.loads(context_data['template_data'])

        stack_parm = template_dict['Parameters']
        highlight_parm = [{'label': k, 'val': v} for (k, v) in stack_parm.iteritems()
                          if 'Label' in v and v['Label'] == self.param_prefix]

        stack_resource = template_dict['Resources']
        resource_name_and_parm = self._get_string_of_resource_and_param(stack_resource, highlight_parm, stack_parm)

        # _map = self.get_res_policy_map(stack_resource, stack_parm)
        policy = dict([(k, v) for k, v in stack_resource.iteritems() if v['Type'] == "AWS::AutoScaling::ScalingPolicy"])
        alarms = dict([(k, v) for k, v in stack_resource.iteritems() if v['Type'] == "OS::Ceilometer::Alarm"])

        policy_dict, map = self.bind_policies_relative(policy, stack_parm, alarms)

        try:
            with chef.autoconfigure():
                chef_role = chef.Role.list()
        except Exception:
            chef_role = []

        meters = api.ceilometer.Meters(self.request)
        if not meters._ceilometer_meter_list:
            msg = _("There are no meters defined yet.")
            messages.warning(self.request, msg)

        meter_names = [meter.name for meter in meters.list_nova()]


        steps = []
        # resource turn here.
        for res, params in resource_name_and_parm:  # 어차피 한개라서 for문 빼고 싶지만, 일단 잘 되니까 냅두자.
            param_ = [k for k in params if k['label'] != 'KeyName']  # KeyName HardCode.
            param_.extend([v for v in highlight_parm if v['label'] == 'Volume']) # hardcode 201406161546
            chef_role_determine_list = [bool(re.search('\w*RoleName', x['label'])) for x in param_]

            steps.append({
                'type': 'resource',
                'value': {
                    'name': res,
                    'param': param_,
                    'chef_role': param_[chef_role_determine_list.index(True)]['label'] if True in chef_role_determine_list else '',
                    'policy': policy_dict(res, map),
                    'tmpl': res + ':' + json.dumps(stack_resource[res], sort_keys=True, indent=4),
                    'allow_role_list': self.filter_chef_role(chef_role, res),
                    'allow_meter_list': meter_names
                }})
        # confirm turn here.
        confirm = {'type': 'confirm', 'value': {'name': 'confirm'}}
        # essential turn here.
        key_name_ = [k for k in highlight_parm if k['label'] == 'KeyName']
        essential = {
            'type': 'essential',
            'value': {
                'name': 'essential',
                'param': key_name_,  # KeyName HardCode.
                'tmpl': 'KeyName :' + json.dumps(key_name_, sort_keys=True, indent=4)
            }}

        context_data['display_type'] = self.request.POST['display_type'] \
            if self.request.method.lower() == 'post' else self.request.GET['display_type']
        context_data['chef_role'] = chef_role
        context_data['prefix'] = self.param_prefix
        context_data['chef_role_prefix'] = self.chef_role_prefix
        context_data['steps'] = steps
        context_data['essential'] = essential
        context_data['confirm'] = confirm

        keypairs = api.nova.keypair_list(self.request)
        context_data['key_list'] = [k.name for k in keypairs]

        return context_data