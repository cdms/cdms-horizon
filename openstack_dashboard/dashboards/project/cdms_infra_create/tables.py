# coding=utf-8
# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 Nebula, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
#

from horizon import tables


class InfraTemplate(object):
    def __init__(self, id_, template_name, template_path):
        self.id = id_
        self.template_name = template_name
        self.template_path = template_path


class CreateStack(tables.LinkAction):
    name = "create_stack"
    verbose_name = "Deploy"

    def get_link_url(self, datum=None):
        return "/project/cdms_infra_create/create/%s?display_type=infra" % datum.template_name


class InfraStacksTable(tables.DataTable):
    name = tables.Column("template_name", verbose_name="Template name", link='horizon:project:cdms_infra_create:show')

    def get_object_display(self, datum):
        return datum.template_name

    class Meta:
        name = "infra_template"
        verbose_name = " "
        row_actions = (CreateStack,)