# coding=utf-8
# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
# Copyright 2012 Nebula, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
"""
Views for cdms project front.
"""
import re
import subprocess

import chef
from django.views.generic import TemplateView
from django.conf import settings

from openstack_dashboard.dashboards.project.cdms.models import Single


ROLE = """{
  "name": "",
  "description": "",
  "json_class": "Chef::Role",
  "default_attributes": {
  },
  "override_attributes": {
  },
  "chef_type": "role",
  "run_list": [

  ],
  "env_run_lists": {
  }
}"""
ENV = """{
  "name": "",
  "description": "",
  "cookbook_versions": {
  },
  "json_class": "Chef::Environment",
  "chef_type": "environment",
  "default_attributes": {
  },
  "override_attributes": {
  }
}"""
DATA_BAG = """[
]"""


def get_server_url():
    try:
        server_url = str(Single.objects.get(key='chef_server_url').value)
    except Single.DoesNotExist:
        server_url = 'https://172.16.100.70:443'
    except Single.MultipleObjectsReturned:
        server_url = 'https://172.16.100.70:443'
    return server_url


class IndexView(TemplateView):
    template_name = 'project/chef/chef_conf.html'

    def dispatch(self, request, *args, **kwargs):
        if request.method.lower() == 'post':
            post = request.POST
            url_ = post['chef_server_url']
            orm, created = Single.objects.get_or_create(key='chef_server_url')
            setattr(orm, 'value', url_)
            orm.save()

            ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', url_)[0]
            sh_ip = settings.MEDIA_ROOT + '/get_chef_server_info.sh ' + ip
            subprocess.call(sh_ip, shell=True)
            print sh_ip

            request.method = 'GET'
        return super(IndexView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(IndexView, self).get_context_data(**kwargs)
        server_url = get_server_url()
        context_data['chef_server_url'] = server_url

        try:
            with chef.autoconfigure():
                addable_chef_list = [
                    {
                        'title': 'ROLE LIST',
                        'list': chef.Role.list(),
                        'tmpl': ROLE
                    },
                    {
                        'title': 'DATA BAG LIST',
                        'list': chef.DataBag.list(),
                        'tmpl': DATA_BAG
                    },
                    {
                        'title': 'ENV LIST',
                        'list': chef.Environment.list(),
                        'tmpl': ENV
                    }
                ]
                cookbooks = chef.ChefAPI.get_global().api_request(
                    'GET', '/cookbooks?num_versions=all&ignore_environments=true')

        except Exception:
            addable_chef_list = [
                {
                    'title': 'ROLE LIST',
                    'list': [],
                    'tmpl': ROLE
                },
                {
                    'title': 'DATA BAG LIST',
                    'list': [],
                    'tmpl': DATA_BAG
                },
                {
                    'title': 'ENV LIST',
                    'list': [],
                    'tmpl': ENV
                }
            ]
            cookbooks = []

        context_data['addable_chef_list'] = addable_chef_list
        context_data['cookbooks'] = cookbooks
        return context_data


class JSONLintView(TemplateView):
    template_name = 'project/chef/jsonlint.html'