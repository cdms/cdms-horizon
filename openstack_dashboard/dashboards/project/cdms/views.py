# coding=utf-8
# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
# Copyright 2012 Nebula, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
"""
Views for cdms project front.
"""

from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import View
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from horizon import exceptions
from horizon import tables
from horizon import tabs
from horizon.utils import memoized
from openstack_dashboard import api
from openstack_dashboard.dashboards.project.cdms \
    import tables as project_tables
from openstack_dashboard.dashboards.project.cdms \
    import tabs as project_tabs


class IndexView(tables.DataTableView):
    table_class = project_tables.StacksTable
    template_name = 'project/cdms/index.html'

    def get_data(self):
        request = self.request
        try:
            stacks = [stack for stack in api.heat.stacks_list(self.request) if stack.stack_name not in('Chef_Server', 'chef-server')]
            stacks = [api.heat.stack_get(self.request, stack.id) for stack in stacks]
        except Exception:
            exceptions.handle(request, _('Unable to retrieve stack list.'))
            stacks = []
        return stacks


class DetailView(View):
    def dispatch(self, request, *args, **kwargs):
        stack = self.get_data(self.request, **kwargs)
        if getattr(stack, 'outputs', None):
            outputs = stack.outputs
            if len(outputs) > 1:  # TODO 일단 하드 코드.
                return HttpResponseRedirect('/project/cdms/app/%s' % stack.id)
            else:
                return HttpResponseRedirect('/project/cdms/infra/%s' % stack.id)
        else:
            return HttpResponseRedirect('/project/cdms/')

    @memoized.memoized_method
    def get_data(self, request, **kwargs):
        stack_id = kwargs['stack_id']
        try:
            stack = api.heat.stack_get(request, stack_id)
            request.session['stack_id'] = stack.id
            request.session['stack_name'] = stack.stack_name
            return stack
        except Exception:
            msg = _("Unable to retrieve stack.")
            redirect = reverse('horizon:project:cdms:index')
            exceptions.handle(request, msg, redirect=redirect)


class AppDetailView(tabs.TabView):
    tab_group_class = project_tabs.AppDetailTabs
    template_name = 'project/cdms/app_detail.html'

    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax() and 'hanjong' in request.GET:
            stack_id = kwargs['stack_id']
            stack = api.heat.stack_get(request, stack_id)
            return HttpResponse(project_tabs.get_detail_view_json(request, stack), content_type="application/json")
        return super(AppDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AppDetailView, self).get_context_data(**kwargs)
        context["stack"] = self.get_data(self.request, **kwargs)
        return context

    @memoized.memoized_method
    def get_data(self, request, **kwargs):
        stack_id = kwargs['stack_id']
        try:
            stack = api.heat.stack_get(request, stack_id)
            request.session['stack_id'] = stack.id
            request.session['stack_name'] = stack.stack_name
            return stack
        except Exception:
            msg = _("Unable to retrieve stack.")
            redirect = reverse('horizon:project:stacks:index')
            exceptions.handle(request, msg, redirect=redirect)

    def get_tabs(self, request, **kwargs):
        stack = self.get_data(request, **kwargs)
        return self.tab_group_class(request, stack=stack, **kwargs)


class InfraDetailView(tabs.TabView):
    tab_group_class = project_tabs.InfraDetailTabs
    template_name = 'project/cdms/infra_detail.html'

    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax() and 'hanjong' in request.GET:
            stack_id = kwargs['stack_id']
            stack = api.heat.stack_get(request, stack_id)

            # HARD CODE for gluster
            gluster_stacks = project_tabs.find_stacks_by_name(request, 'gluster')
            stacks = []
            for gluster_stack in gluster_stacks:
                stacks.append(api.heat.stack_get(request,gluster_stack))
            return HttpResponse(project_tabs.get_storage_detail_view_json(request, stack.id, stacks), content_type="application/json")
        return super(InfraDetailView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(InfraDetailView, self).get_context_data(**kwargs)
        context["stack"] = self.get_data(self.request, **kwargs)
        return context

    @memoized.memoized_method
    def get_data(self, request, **kwargs):
        stack_id = kwargs['stack_id']
        try:
            stack = api.heat.stack_get(request, stack_id)
            request.session['stack_id'] = stack.id
            request.session['stack_name'] = stack.stack_name
            return stack
        except Exception:
            msg = _("Unable to retrieve stack.")
            redirect = reverse('horizon:project:stacks:index')
            exceptions.handle(request, msg, redirect=redirect)

    def get_tabs(self, request, **kwargs):
        stack = self.get_data(request, **kwargs)
        return self.tab_group_class(request, stack=stack, **kwargs)