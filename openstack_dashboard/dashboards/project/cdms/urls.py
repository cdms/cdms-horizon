# coding=utf-8
# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
# Copyright 2012 Nebula, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#

from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa

from openstack_dashboard.dashboards.project.cdms import views


VIEW_MOD = 'openstack_dashboard.dashboards.project.cdms.views'

urlpatterns = patterns(VIEW_MOD,
   url(r'^$', views.IndexView.as_view(), name='index'),

   url(r'^stack/(?P<stack_id>[^/]+)/$', views.DetailView.as_view(), name='detail'), #table에서 연결되는 list인데, 이 URL 모양을 바꿀 수 없음;
   url(r'^infra/(?P<stack_id>[^/]+)/$', views.InfraDetailView.as_view(), name='infra_detail'),
   url(r'^app/(?P<stack_id>[^/]+)/$', views.AppDetailView.as_view(), name='app_detail'),
)




