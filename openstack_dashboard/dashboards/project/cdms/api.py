# coding=utf-8
from openstack_dashboard import api

__author__ = 'rino0601'

import json


def get_json(key, request):
    ret_dict = None
    if key == 'list':
        ret_dict = [
            {
                'name': stack.stack_name,
                'id': stack.id,
                'status': stack.stack_status
            } for stack in api.heat.stacks_list(request) if stack.stack_name != 'chef-server'
        ]
    elif key == 'reflect':  # 희대의 시간낭비. 왜 했지...
        post = request.POST
        ret_dict = {}
        for k in post:
            post_getlist = post.getlist(k)
            ret_dict[k] = post_getlist if len(post_getlist) > 1 else post_getlist[0]

    return json.dumps(ret_dict)