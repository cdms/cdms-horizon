# coding=utf-8
# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
import json
import logging
import re
import chef

from datetime import datetime

from django.utils.datastructures import SortedDict
from django.utils.translation import ugettext_lazy as _

from horizon import exceptions
from horizon import messages
from horizon import tabs
from openstack_dashboard import api
from openstack_dashboard.dashboards.project.stacks \
    import api as project_api
from openstack_dashboard.dashboards.project.stacks import mappings
from openstack_dashboard.dashboards.project.stacks \
    import tables as project_tables
from openstack_dashboard.dashboards.project.cdms \
    import tables as cdms_tables


LOG = logging.getLogger(__name__)

def get_storage_detail_view_json(request, my_id, stacks):
    def get_policy_string(scale_out, scale_in):
        s = ''
        if 'meter_name' in scale_out:
            s += 'UP('
            s += 'meter:' + scale_out['meter_name']
            s += ', period:' + scale_out['period']
            s += ', threshold:' + scale_out['threshold']
            s += ') '

        if 'meter_name' in scale_in:
            s += 'DOWN('
            s += 'meter:' + scale_in['meter_name']
            s += ', period:' + scale_in['period']
            s += ', threshold:' + scale_in['threshold']
            s += ') '
        return s

    def get_flavor_string(id_, flavors):
        for f in [f for f in flavors if f.id == str(id_)]:
            s = ''
            s += f.name
            s += ' / RAM: %rMB' % f.ram
            s += ' / vCPU: %r' % f.vcpus
            s += ' / DISK: %rGB' % f.disk
            return s
        return 'unknown'

    server_list, foo = api.nova.server_list(request)
    flavors = api.nova.flavor_list(request, None)

    outputs = []
    for stack in stacks:
        if getattr(stack, 'outputs', None):
            for output in stack.outputs:
                json_loads = json.loads(output['output_value'])
                outputs.append({
                    'output_key': output['output_key'],
                    'ips': json_loads['ips'].split(','),
                    'role': json_loads['role'],
                    'min': json_loads['min'],
                    'max': json_loads['max'],
                    'scale-out': json_loads['scale-out'],
                    'scale-in': json_loads['scale-in'],
                    'databag': json_loads['databag'],
                    'volume': json_loads['volume'],
                    'stack_name': stack.stack_name,
                    'stack_id': stack.id,
                    'is_mine': 1 if my_id == stack.id else 0
                })  # now, output in outputs has all value from stack.outputs.
    data = {
        'timestamp': str(datetime.now()),
        'layers': [
            {
                'stack_id': output['stack_id'],
                'members': [
                    {
                        'id': server.id,
                        'name': server.name,
                        'ip': server.addresses['private'][0]['addr'],
                        'role': output['role'],
                        'console_url': api.nova.server_vnc_console(request, server.id).url,
                        'layer_name': output['output_key'],
                        'flavor_str': get_flavor_string(server.flavor['id'], flavors),
                        'policy_str': get_policy_string(output['scale-out'], output['scale-in']),
                        'sample': '%0.2f%%'%(
                        api.ceilometer.get_sample(request, server.id, output['scale-out']['meter_name'])[0].counter_volume),
                        'up_policy': {
                            'meter_name': output['scale-out']['meter_name'],
                            'period': (str(output['scale-out']['period']+' seconds') if len(output['scale-out']['period']) > 0 else ''),
                            'threshold': (str(output['scale-out']['threshold']+' %') if len(output['scale-out']['threshold']) > 0 else ''),
                            'description': output['scale-out']['description']
                        }
                    } for server in server_list if server.addresses['private'][0]['addr'] in output['ips']],
                'name': output['stack_name'],
                'scale-out': output['scale-out'],
                'scale-in': output['scale-in'],
                'databag_name': output['databag'],
                'volume': output['volume'],
                'volume_info': str(_databag_init_node_get(output['databag']) + ':' + output['volume']),
                'stack_name': output['stack_name'],
                'is_mine': output['is_mine']
            } for output in outputs
        ]
    }
    return json.dumps(data)

def get_api():
    return chef.autoconfigure()

def _databag_init_node_get(databag, item_name='config'):
    api = get_api()
    try:
        db = _databag_item_list(databag, item_name)
        return db['init_node']
    except Exception:
        LOG.info(_('ChefServer Databag item list error'))

    return ""

def _databag_item_list(databag_name, item_name):
    api = get_api()
    url = '/data/' + databag_name + '/' + item_name
    try:
        data = dict(api.api_request('GET', url))
    except Exception:
        LOG.info(_('ChefServer Databag item list error'))
        data = {}
    return data

def get_detail_view_json(request, stack):
    def get_policy_string(scale_out, scale_in):
        s = ''
        if 'meter_name' in scale_out:
            s += 'UP('
            s += 'meter:' + scale_out['meter_name']
            s += ', period:' + scale_out['period']
            s += ', threshold:' + scale_out['threshold']
            s += ') '

        if 'meter_name' in scale_in:
            s += 'DOWN('
            s += 'meter:' + scale_in['meter_name']
            s += ', period:' + scale_in['period']
            s += ', threshold:' + scale_in['threshold']
            s += ') '
        return s

    def get_flavor_string(id_, flavors):
        for f in [f for f in flavors if f.id == str(id_)]:
            s = ''
            s += f.name
            s += ' / RAM: %rMB' % f.ram
            s += ' / vCPU: %r' % f.vcpus
            s += ' / DISK: %rGB' % f.disk
            return s
        return 'unknown'

    server_list, foo = api.nova.server_list(request)
    flavors = api.nova.flavor_list(request, None)

    ret = {}
    if getattr(stack, 'outputs', None):
        outputs = []
        for output in stack.outputs:
            json_loads = json.loads(output['output_value'])
            outputs.append({
                'output_key': output['output_key'],
                'ips': json_loads['ips'].split(','),
                'role': json_loads['role'],
                'min': json_loads['min'],
                'max': json_loads['max'],
                'scale-out': json_loads['scale-out'],
                'scale-in': json_loads['scale-in']
            })  # now, output in outputs has all value from stack.outputs.
            # todo enhance this!, log this!

        # hard code 201406171720 force ordering.
        LoadBalancer = [l for l in outputs if l['output_key'] == 'LoadBalancer']
        WebServer = [l for l in outputs if l['output_key'] == 'WebServer']
        Database = [l for l in outputs if l['output_key'] == 'Database']
        outputs = [LoadBalancer[0], WebServer[0], Database[0]]

        data = {
            'timestamp': str(datetime.now()),
            'layers': [
                {
                    'stack_id': stack.id,
                    'members': [
                        {
                            'id': server.id,
                            'name': server.name,
                            'ip': server.addresses['private'][0]['addr'],
                            'role': output['role'],
                            'console_url': api.nova.server_vnc_console(request, server.id).url,
                            'layer_name': output['output_key'],
                            'flavor_str': get_flavor_string(server.flavor['id'], flavors),
                            'policy_str': get_policy_string(output['scale-out'], output['scale-in']),
                            'sample': ('%0.2f%%'%(
                            api.ceilometer.get_sample(request, server.id, output['scale-out']['meter_name'])[0].counter_volume)) if output['output_key'] == 'WebServer' else '',
                            'up_policy': {
                                'meter_name': output['scale-out']['meter_name'],
                                'period': str(output['scale-out']['period']+' seconds') if len(str(output['scale-out']['period'])) > 1 else '',
                                'threshold': str(output['scale-out']['threshold']+' %') if len(str(output['scale-out']['threshold'])) > 1 else '',
                                'description': output['scale-out']['description']
                            },
                            'down_policy': {
                                'meter_name': output['scale-in']['meter_name'],
                                'period': str(output['scale-in']['period']+' seconds') if len(str(output['scale-in']['period'])) > 1 else '',
                                'threshold': str(output['scale-in']['threshold']+' %') if len(str(output['scale-in']['threshold'])) > 1 else '',
                                'description': output['scale-in']['description']
                            }
                        } for server in server_list if server.addresses['private'][0]['addr'] in output['ips']],
                    'name': output['output_key'],
                    'scale-out': output['scale-out'],
                    'scale-in': output['scale-in']
                } for output in outputs
            ]
        }
        ret.update(data)
    else:
        data = {
            'timestamp': str(datetime.now()),
            'layers': []
        }
        ret.update(data)
    return json.dumps(ret)

# JiHyun
def find_stacks_by_name(request, name='gluster'):
    stacks_list = api.heat.stacks_list(request)
    names = []
    pt_name = re.compile(name)
    for stack in stacks_list:
        if pt_name.match(stack.stack_name):
            names.append(stack.id)

    return names



"""
COMMON CLASS FROM HERE ~~~
"""


class StackTopologyTab(tabs.Tab):
    name = _("Topology")
    slug = "topology"
    template_name = "project/stacks/_detail_topology.html"

    def get_context_data(self, request):
        context = {}
        stack = self.tab_group.kwargs['stack']
        context['stack_id'] = stack.id
        context['d3_data'] = project_api.d3_data(request, stack_id=stack.id)
        return context


class StackOverviewTab(tabs.Tab):
    name = _("Overview")
    slug = "overview"
    template_name = "project/stacks/_detail_overview.html"

    def get_context_data(self, request):
        return {"stack": self.tab_group.kwargs['stack']}


class ResourceOverviewTab(tabs.Tab):
    name = _("Overview")
    slug = "resource_overview"
    template_name = "project/stacks/_resource_overview.html"
    preload = False

    def get_context_data(self, request):
        resource = self.tab_group.kwargs['resource']
        resource_url = mappings.resource_to_url(resource)
        return {
            "resource": resource,
            "resource_url": resource_url,
            "metadata": self.tab_group.kwargs['metadata']}


class StackEventsTab(tabs.Tab):
    name = _("Events")
    slug = "events"
    template_name = "project/stacks/_detail_events.html"
    preload = False

    def get_context_data(self, request):
        stack = self.tab_group.kwargs['stack']
        try:
            stack_identifier = '%s/%s' % (stack.stack_name, stack.id)
            events = api.heat.events_list(self.request, stack_identifier)
            LOG.debug('got events %s' % events)
        except Exception:
            events = []
            messages.error(request, _(
                'Unable to get events for stack "%s".') % stack.stack_name)
        return {"stack": stack,
                "table": project_tables.EventsTable(request, data=events), }


class StackResourcesTab(tabs.Tab):
    name = _("Resources")
    slug = "resources"
    template_name = "project/stacks/_detail_resources.html"
    preload = False

    def get_context_data(self, request):
        stack = self.tab_group.kwargs['stack']
        try:
            stack_identifier = '%s/%s' % (stack.stack_name, stack.id)
            resources = api.heat.resources_list(self.request, stack_identifier)
            LOG.debug('got resources %s' % resources)
        except Exception:
            resources = []
            messages.error(request, _(
                'Unable to get resources for stack "%s".') % stack.stack_name)
        return {"stack": stack,
                "table": project_tables.ResourcesTable(
                    request, data=resources, stack=stack), }


class ResourceDetailTabs(tabs.TabGroup):
    slug = "resource_details"
    tabs = (ResourceOverviewTab,)
    sticky = True


"""
~~~~ TO HERE.
"""

"""
TAB
"""


class InfraDetailTab(tabs.Tab):
    name = "Deploy View"
    slug = "infra_shape"
    template_name = "project/cdms/infra_project_detail.html"

    def get_data(self, ids):
        try:
            instances, dump = api.nova.server_list(
                self.request,
                search_opts={'marker': None,
                             'paginate': True})
        except Exception:
            instances = []
            exceptions.handle(self.request,
                              _('Unable to retrieve instances.'))

        if instances:
            try:
                api.network.servers_update_addresses(self.request, instances)
            except Exception:
                exceptions.handle(
                    self.request,
                    message=_('Unable to retrieve IP addresses from Neutron.'),
                    ignore=True)

            # Gather our flavors and images and correlate our instances to them
            try:
                flavors = api.nova.flavor_list(self.request)
            except Exception:
                flavors = []
                exceptions.handle(self.request, ignore=True)

            try:
                # TODO(gabriel): Handle pagination.
                images, more = api.glance.image_list_detailed(self.request)
            except Exception:
                images = []
                exceptions.handle(self.request, ignore=True)

            full_flavors = SortedDict([(str(flavor.id), flavor)
                                       for flavor in flavors])
            image_map = SortedDict([(str(image.id), image)
                                    for image in images])

            # Loop through instances to get flavor info.
            for instance in instances:
                if hasattr(instance, 'image'):
                    # Instance from image returns dict
                    if isinstance(instance.image, dict):
                        if instance.image.get('id') in image_map:
                            instance.image = image_map[instance.image['id']]
                    else:
                        # Instance from volume returns a string
                        instance.image = {'name': instance.image if instance.image else _("-")}

                try:
                    flavor_id = instance.flavor["id"]
                    if flavor_id in full_flavors:
                        instance.full_flavor = full_flavors[flavor_id]
                    else:
                        # If the flavor_id is not in full_flavors list,
                        # get it via nova api.
                        instance.full_flavor = api.nova.flavor_get(
                            self.request, flavor_id)
                except Exception:
                    msg = _('Unable to retrieve instance size information.')
                    exceptions.handle(self.request, msg)

        return [inst for inst in instances if inst.id in ids]  # instances

    def get_table_data(self, context_data):
        domain_data = []
        layers_ = json.loads(context_data['layers_json'])['layers']
        members_ = [layer['members'] for layer in layers_]
        for member in members_:
            domain_data.extend(member)
        resources = [InfraDetailTab.TableData(data['id'], data['name'], data['ip'], data['role'], data['layer_name'])
                     for data in domain_data]
        return resources

    def get_context_data(self, request):
        stack = self.tab_group.kwargs['stack']

        # HARD CODE for gluster
        gluster_stacks = find_stacks_by_name(request, 'gluster')

        stacks = []
        for gluster_stack in gluster_stacks:
            stacks.append(api.heat.stack_get(request,gluster_stack))


        context_data = {
            'layers_json': get_storage_detail_view_json(request, stack.id, stacks)
        }

        # HARD CODE fin
        #context_data = {
        #    'layers_json': get_detail_view_json(request, stack)
        #}

        temp = json.loads(context_data['layers_json'])
        meters = []
        ids = []
        for a in temp['layers']:
            ids.extend([i['id'] for i in a['members']])
            meters.append(a['scale-out'].get('meter_name', None))
            meters.append(a['scale-in'].get('meter_name', None))
        context_data['meters'] = set([a for a in meters if a])  # template에 한번 찍어보자... for문 잘 도는듯...?

        meters = api.ceilometer.Meters(self.request)
        if not meters._ceilometer_meter_list:
            msg = _("There are no meters defined yet.")
            messages.warning(self.request, msg)

        context = {
            'nova_meters': meters.list_nova(),
            'neutron_meters': meters.list_neutron(),
            'glance_meters': meters.list_glance(),
            'cinder_meters': meters.list_cinder(),
            'swift_meters': meters.list_swift(),
            'kwapi_meters': meters.list_kwapi(),
        }
        context_data.update(context)

        identified_ids =[]

        for b in temp['layers']:
            if b['is_mine'] == 1:
                identified_ids.extend([i['id'] for i in b['members']])
            
        resources = self.get_data(identified_ids)

        base_ = {"stack": stack,
                 "table": cdms_tables.CDMSDetailInfraTable(
                     request, data=resources, stack=stack), }

        context_data.update(base_)
        return context_data

    class TableData(object):

        def __init__(self, id_, name, ip, chef_role, layer):
            self.id = id_
            self.chef_role = chef_role
            self.ip = ip
            self.name = name
            self.layer = layer


class AppDetailTab(tabs.Tab):
    name = "Deploy View"
    slug = "app_shape"
    template_name = "project/cdms/app_project_detail.html"

    def get_data(self, ids):
        try:
            instances, dump = api.nova.server_list(
                self.request,
                search_opts={'marker': None,
                             'paginate': True})
        except Exception:
            instances = []
            exceptions.handle(self.request,
                              _('Unable to retrieve instances.'))

        if instances:
            try:
                api.network.servers_update_addresses(self.request, instances)
            except Exception:
                exceptions.handle(
                    self.request,
                    message=_('Unable to retrieve IP addresses from Neutron.'),
                    ignore=True)

            # Gather our flavors and images and correlate our instances to them
            try:
                flavors = api.nova.flavor_list(self.request)
            except Exception:
                flavors = []
                exceptions.handle(self.request, ignore=True)

            try:
                # TODO(gabriel): Handle pagination.
                images, more = api.glance.image_list_detailed(self.request)
            except Exception:
                images = []
                exceptions.handle(self.request, ignore=True)

            full_flavors = SortedDict([(str(flavor.id), flavor)
                                       for flavor in flavors])
            image_map = SortedDict([(str(image.id), image)
                                    for image in images])

            # Loop through instances to get flavor info.
            for instance in instances:
                if hasattr(instance, 'image'):
                    # Instance from image returns dict
                    if isinstance(instance.image, dict):
                        if instance.image.get('id') in image_map:
                            instance.image = image_map[instance.image['id']]
                    else:
                        # Instance from volume returns a string
                        instance.image = {'name': instance.image if instance.image else _("-")}

                try:
                    flavor_id = instance.flavor["id"]
                    if flavor_id in full_flavors:
                        instance.full_flavor = full_flavors[flavor_id]
                    else:
                        # If the flavor_id is not in full_flavors list,
                        # get it via nova api.
                        instance.full_flavor = api.nova.flavor_get(
                            self.request, flavor_id)
                except Exception:
                    msg = _('Unable to retrieve instance size information.')
                    exceptions.handle(self.request, msg)

        return [inst for inst in instances if inst.id in ids]  # instances

    def get_table_data(self, context_data):
        domain_data = []
        layers_ = json.loads(context_data['layers_json'])['layers']
        members_ = [layer['members'] for layer in layers_]
        for member in members_:
            domain_data.extend(member)
        resources = [AppDetailTab.TableData(data['id'], data['name'], data['ip'], data['role'], data['layer_name'])
                     for data in domain_data]
        return resources

    def get_context_data(self, request):
        stack = self.tab_group.kwargs['stack']
        context_data = {
            'layers_json': get_detail_view_json(request, stack)
        }
        temp = json.loads(context_data['layers_json'])
        meters = []
        ids = []
        for a in temp['layers']:
            ids.extend([i['id'] for i in a['members']])
            meters.append(a['scale-out'].get('meter_name', None))
            meters.append(a['scale-in'].get('meter_name', None))
        context_data['meters'] = set([a for a in meters if a])  # template에 한번 찍어보자... for문 잘 도는듯...?

        meters = api.ceilometer.Meters(self.request)
        if not meters._ceilometer_meter_list:
            msg = _("There are no meters defined yet.")
            messages.warning(self.request, msg)

        context = {
            'nova_meters': meters.list_nova(),
            'neutron_meters': meters.list_neutron(),
            'glance_meters': meters.list_glance(),
            'cinder_meters': meters.list_cinder(),
            'swift_meters': meters.list_swift(),
            'kwapi_meters': meters.list_kwapi(),
        }
        context_data.update(context)

        # resources = self.get_table_data(context_data)
        resources = self.get_data(ids)

        base_ = {"stack": stack,
                 "table": cdms_tables.CDMSDetailAppTable(
                     request, data=resources, stack=stack), }

        context_data.update(base_)
        return context_data

    class TableData(object):

        def __init__(self, id_, name, ip, chef_role, layer):
            self.id = id_
            self.chef_role = chef_role
            self.ip = ip
            self.name = name
            self.layer = layer


class InfraDetailTabs(tabs.TabGroup):
    slug = "Infra_details"
    tabs = (StackTopologyTab, InfraDetailTab, StackOverviewTab, StackResourcesTab,
            StackEventsTab)
    sticky = True


class AppDetailTabs(tabs.TabGroup):
    slug = "app_details"
    tabs = (AppDetailTab, StackOverviewTab, StackResourcesTab, StackEventsTab)
    # tabs = ( StackTopologyTab, AppDetailTab, StackOverviewTab, StackResourcesTab, StackEventsTab)
    sticky = True
