# coding=utf-8
# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 Nebula, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
#
from django.http import Http404  # noqa
from django.template.defaultfilters import title  # noqa
from django.utils.translation import ugettext_lazy as _
from django import template

from horizon import messages
from heatclient import exc
from openstack_dashboard import api
from horizon import tables
from horizon.utils import filters
from horizon.templatetags import sizeformat

POWER_STATES = {
    0: "NO STATE",
    1: "RUNNING",
    2: "BLOCKED",
    3: "PAUSED",
    4: "SHUTDOWN",
    5: "SHUTOFF",
    6: "CRASHED",
    7: "SUSPENDED",
    8: "FAILED",
    9: "BUILDING",
}

TASK_STATUS_CHOICES = (
    (None, True),
    ("none", True)
)
STATUS_CHOICES = (
    ("active", True),
    ("shutoff", True),
    ("suspended", True),
    ("paused", True),
    ("error", False),
)
STATUS_DISPLAY_CHOICES = (
    ("resize", _("Resize/Migrate")),
    ("verify_resize", _("Confirm or Revert Resize/Migrate")),
    ("revert_resize", _("Revert Resize/Migrate")),
)
TASK_DISPLAY_CHOICES = (
    ("image_snapshot", _("Snapshotting")),
    ("resize_prep", _("Preparing Resize or Migrate")),
    ("resize_migrating", _("Resizing or Migrating")),
    ("resize_migrated", _("Resized or Migrated")),
    ("resize_finish", _("Finishing Resize or Migrate")),
    ("resize_confirming", _("Confirming Resize or Migrate")),
    ("resize_reverting", _("Reverting Resize or Migrate")),
    ("unpausing", _("Resuming")),
)


def get_ips(instance):
    template_name = 'project/instances/_instance_ips.html'
    context = {"instance": instance}
    return template.loader.render_to_string(template_name, context)


def get_size(instance):
    if hasattr(instance, "full_flavor"):
        size_string = _("%(name)s | %(RAM)s RAM | %(VCPU)s VCPU "
                        "| %(disk)s Disk")
        vals = {'name': instance.full_flavor.name,
                'RAM': sizeformat.mbformat(instance.full_flavor.ram),
                'VCPU': instance.full_flavor.vcpus,
                'disk': sizeformat.diskgbformat(instance.full_flavor.disk)}
        return size_string % vals
    return _("Not available")


def get_keyname(instance):
    if hasattr(instance, "key_name"):
        keyname = instance.key_name
        return keyname
    return _("Not available")


def get_power_state(instance):
    return POWER_STATES.get(getattr(instance, "OS-EXT-STS:power_state", 0), '')


class JSAction(tables.Action):
    name = "example"
    verbose_name = "show more"
    verbose_name_plural = "show more"

    def allowed(self, request, obj=None):
        return getattr(obj, 'status', None) != 'down'

    def handle(self, data_table, request, object_ids):
        return None




################
class CDMSDetailInfraTable(tables.DataTable):
    name = tables.Column("name",
                         link=("horizon:project:instances:detail"),
                         verbose_name=_("Instance Name"))
    image_name = tables.Column("image_name",
                               verbose_name=_("Image Name"))
    ip = tables.Column(get_ips,
                       verbose_name=_("IP Address"),
                       attrs={'data-type': "ip"})
    size = tables.Column(get_size,
                         verbose_name=_("Size"),
                         attrs={'data-type': 'size'})
    keypair = tables.Column(get_keyname, verbose_name=_("Key Pair"))
    status = tables.Column("status",
                           filters=(title, filters.replace_underscores),
                           verbose_name=_("Status"),
                           status=True,
                           status_choices=STATUS_CHOICES,
                           display_choices=STATUS_DISPLAY_CHOICES)
    az = tables.Column("availability_zone",
                       verbose_name=_("Availability Zone"))
    task = tables.Column("OS-EXT-STS:task_state",
                         verbose_name=_("Task"),
                         filters=(title, filters.replace_underscores),
                         status=True,
                         status_choices=TASK_STATUS_CHOICES,
                         display_choices=TASK_DISPLAY_CHOICES)
    state = tables.Column(get_power_state,
                          filters=(title, filters.replace_underscores),
                          verbose_name=_("Power State"))
    created = tables.Column("created",
                            verbose_name=_("Uptime"),
                            filters=(filters.parse_isotime,
                                     filters.timesince_sortable),
                            attrs={'data-type': 'timesince'})

    def __init__(self, request, data=None, needs_form_wrapper=None, **kwargs):
        super(CDMSDetailInfraTable, self).__init__(request, data, needs_form_wrapper, **kwargs)
        self.stack = kwargs['stack']

    class Meta:
        name = "instances"
        verbose_name = "Volumes"
        row_actions = (JSAction,)
################
class CDMSDetailAppTable(tables.DataTable):
    name = tables.Column("name",
                         link=("horizon:project:instances:detail"),
                         verbose_name=_("Instance Name"))
    image_name = tables.Column("image_name",
                               verbose_name=_("Image Name"))
    ip = tables.Column(get_ips,
                       verbose_name=_("IP Address"),
                       attrs={'data-type': "ip"})
    size = tables.Column(get_size,
                         verbose_name=_("Size"),
                         attrs={'data-type': 'size'})
    keypair = tables.Column(get_keyname, verbose_name=_("Key Pair"))
    status = tables.Column("status",
                           filters=(title, filters.replace_underscores),
                           verbose_name=_("Status"),
                           status=True,
                           status_choices=STATUS_CHOICES,
                           display_choices=STATUS_DISPLAY_CHOICES)
    az = tables.Column("availability_zone",
                       verbose_name=_("Availability Zone"))
    task = tables.Column("OS-EXT-STS:task_state",
                         verbose_name=_("Task"),
                         filters=(title, filters.replace_underscores),
                         status=True,
                         status_choices=TASK_STATUS_CHOICES,
                         display_choices=TASK_DISPLAY_CHOICES)
    state = tables.Column(get_power_state,
                          filters=(title, filters.replace_underscores),
                          verbose_name=_("Power State"))
    created = tables.Column("created",
                            verbose_name=_("Uptime"),
                            filters=(filters.parse_isotime,
                                     filters.timesince_sortable),
                            attrs={'data-type': 'timesince'})

    def __init__(self, request, data=None, needs_form_wrapper=None, **kwargs):
        super(CDMSDetailAppTable, self).__init__(request, data, needs_form_wrapper, **kwargs)
        self.stack = kwargs['stack']

    class Meta:
        name = "instances"
        verbose_name = _("Instances")
        row_actions = (JSAction,)
################

class DeleteStack(tables.BatchAction):
    name = "delete"
    action_present = _("Delete")
    action_past = _("Scheduled deletion of %(data_type)s")
    data_type_singular = _("Stack")
    data_type_plural = _("Stacks")
    classes = ('btn-danger', 'btn-terminate')

    def action(self, request, stack_id):
        api.heat.stack_delete(request, stack_id)

    def allowed(self, request, stack):
        if stack is not None:
            return stack.stack_status != 'DELETE_COMPLETE'
        return True


class LaunchAppStack(tables.LinkAction):
    name = "launch_app"
    verbose_name = "애플리케이션 생성"
    url = "/project/cdms/stack/choice/template/?display_type=app"
    classes = ("btn-create", "ajax-modal")


class LaunchInfraStack(tables.LinkAction):
    name = "launch_infra"
    verbose_name = "인프라 생성"
    url = "/project/cdms/stack/choice/template/?display_type=infra"
    classes = ("btn-create", "ajax-modal")


class StacksUpdateRow(tables.Row):
    ajax = True

    def can_be_selected(self, datum):
        return datum.stack_status != 'DELETE_COMPLETE'

    def get_data(self, request, stack_id):
        try:
            return api.heat.stack_get(request, stack_id)
        except exc.HTTPNotFound:
            # returning 404 to the ajax call removes the
            # row from the table on the ui
            raise Http404
        except Exception as e:
            messages.error(request, e)


def determine_app_or_infra(stack):
    # api.heat.stack_get()
    if getattr(stack, 'outputs', None):
        outputs = stack.outputs
        if len(outputs) > 1:
            return "application"
        else:
            return "infra"
    else:
        return "pending..."

class StacksTable(tables.DataTable):
    STATUS_CHOICES = (
        ("Complete", True),
        ("Failed", False),
    )
    name = tables.Column("stack_name",
                         verbose_name=_("Stack Name"),
                         link="horizon:project:cdms:detail", ) #
    created = tables.Column("creation_time",
                            verbose_name=_("Created"),
                            filters=(filters.parse_isotime,
                                     filters.timesince_or_never))
    updated = tables.Column(determine_app_or_infra,
                            verbose_name="kind")

    status = tables.Column("status",
                           filters=(title, filters.replace_underscores),
                           verbose_name=_("Status"),
                           status=True,
                           status_choices=STATUS_CHOICES)

    def get_object_display(self, stack):
        return stack.stack_name

    class Meta:
        name = "stacks"
        verbose_name = " "
        status_columns = ["status", ]
        row_class = StacksUpdateRow
        row_actions = (DeleteStack,)